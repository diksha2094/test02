package test02;

public class Fare {
public double calculateTotal(String[] from,String[] to){
	String[] from1=new String[]{"Leslie"};
	String[] to1=new String[]{"Don Mills"};
	if(from[0].contains("Leslie") && to[0].contains("Don Mills")){
		return 2.5;
	}
	if(from[0].contains("Sheppard") && to[0].contains("Finch Station")){
		return 3.0;
	}
	if(from[0].contains("Don Mills") && to[0].contains("Finch Station")){
		return 3.0;
	}
	if(from[0].contains("Finch") && to[0].contains("Sheppard") && from[1].contains("Leslie") && to[1].contains("Don Mills")){
		return 5.50;
	}
	
	if(from[0].contains("Finch") || to[0].contains("Sheppard") &&  from[1].contains("Sheppard") || to[1].contains("Finch") && from[2].contains("Finch") || to[2].contains("Sheppard")){
		return 6.0;
	}
	return 0.0;
}
}
