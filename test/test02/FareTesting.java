package test02;

import static org.junit.Assert.*;

import org.junit.Test;

public class FareTesting {
	Fare f=new Fare();
	
	//R1 : A one way trip inside Zone1
	@Test
	public void test() {
		
		
		String[] from1=new String[]{"Leslie"};
		String[] to1=new String[]{"Don Mills"};
		
		
		Double fare=f.calculateTotal(from1,to1);
		assertEquals(2.5,fare,0.0);
	}

	//R2 : A one way trip inside Zone2
	@Test
	public void test1() {
		
		String[] from1=new String[]{"Sheppard"};
		String[] to1=new String[]{"Finch Station"};
		
		
		Double fare=f.calculateTotal(from1,to1);
		assertEquals(3.0,fare,0.0);
		
	}

	//R3: A trip between zones
	@Test
	public void test2() {
		String[] from1=new String[]{"Don Mills"};
		String[] to1=new String[]{"Finch Station"};
		Double fare=f.calculateTotal(from1,to1);
		assertEquals(3.0,fare,0.0);
		
	}
	//R4: More than one trip
	@Test
	public void test3() {
		String[] from1=new String[]{"Finch","Leslie"};
		String[] to1=new String[]{"Sheppard","Don Mills"};
		Double fare=f.calculateTotal(from1,to1);
		assertEquals(5.50,fare,0.0);
		
	}
	//R5: Reaching Daily maximum
	@Test
	public void test4() {
		
		String[] from1=new String[]{"Finch","Sheppard","Finch"};
		String[] to1=new String[]{"Sheppard","Finch","Sheppard"};
		Double fare=f.calculateTotal(from1,to1);
		assertEquals(6.0,fare,0.0);
		
	}
}
